package com.todoapi.controllers;

import com.todoapi.exceptions.ResourceNotFound;
import com.todoapi.models.Category;
import com.todoapi.models.Todo;
import com.todoapi.repositories.CategoryRepository;
import com.todoapi.repositories.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    TodoRepository todoRepository;

    // Get All Categories
    @GetMapping("/categories")
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    // Create a new Category
    @PostMapping("/categories")
    public Category createCategory(@Valid @RequestBody Category category) {
        return categoryRepository.save(category);
    }

    // Update a Category
    @PutMapping("/categories/{id}")
    public Category updateCategory(@PathVariable(value = "id") Long categoryId, @Valid @RequestBody Category data) {
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFound("Category", "id", categoryId));
        category.setName(data.getName());
        Category updatedTodo = categoryRepository.save(category);
        return updatedTodo;
    }

    // Delete a Category
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable(value = "id") Long categoryId) {
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFound("Category", "id", categoryId));
        // modify all Todos associated with the category
        // this is dodgy
        for (Todo todo : todoRepository.findByTitleContainsAndCategoryId("",categoryId, null)) {
            todo.setCategory(null);
            todoRepository.save(todo);
        }

        categoryRepository.delete(category);
        return ResponseEntity.ok().build();
    }

}
