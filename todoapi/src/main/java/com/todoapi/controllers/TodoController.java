package com.todoapi.controllers;

import com.todoapi.exceptions.ResourceNotFound;
import com.todoapi.models.Todo;
import com.todoapi.repositories.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TodoController {

    @Autowired
    TodoRepository todoRepository;

    @GetMapping("/todos")
    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    @RequestMapping(value = "/todos/paged", params ={ "page", "title", "category" }, method = RequestMethod.GET)
    @ResponseBody
    public Page<Todo> getAllTodosBy(@RequestParam("page") int page,
                                    @RequestParam("title") String title,
                                    @RequestParam("category") long category) {
        Pageable firstPageWithTwoElements = PageRequest.of((page <= 0)? 0 : page - 1, 5);
        if(category <= 0) {
            return todoRepository.findByTitleContains(title, firstPageWithTwoElements);
        }
        return todoRepository.findByTitleContainsAndCategoryId(title, category, firstPageWithTwoElements);
    }

    // Get a Single TodoItem
    @GetMapping("/todos/{id}")
    public Todo getTodoById(@PathVariable(value = "id") Long todoId) {
        return todoRepository.findById(todoId)
                .orElseThrow(() -> new ResourceNotFound("Todo", "id", todoId));
    }

    // Create a new TodoItem
    @PostMapping("/todos")
    public Todo createTodo(@Valid @RequestBody Todo todo) {
        return todoRepository.save(todo);
    }

    // Update a TodoItem
    @PutMapping("/todos/{id}")
    public Todo updateTodo(@PathVariable(value = "id") Long todoId, @Valid @RequestBody Todo data) {

        Todo todo = todoRepository.findById(todoId)
                .orElseThrow(() -> new ResourceNotFound("Todo", "id", todoId));

        todo.setTitle(data.getTitle());
        todo.setDescription(data.getDescription());
        todo.setIsComplete(data.getIsComplete());
        todo.setCategory(data.getCategory());
        todo.setDueDate(data.getDueDate());

        Todo updatedTodo = todoRepository.save(todo);
        return updatedTodo;
    }

    // Delete a TodoItem
    @DeleteMapping("/todos/{id}")
    public ResponseEntity<?> deleteTodo(@PathVariable(value = "id") Long todoId) {
        Todo todo = todoRepository.findById(todoId)
                .orElseThrow(() -> new ResourceNotFound("Todo", "id", todoId));

        todoRepository.delete(todo);

        return ResponseEntity.ok().build();
    }

}
