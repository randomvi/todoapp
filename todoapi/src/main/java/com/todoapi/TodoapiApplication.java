package com.todoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class TodoapiApplication {
	public static void main(String[] args) {
		SpringApplication.run(TodoapiApplication.class, args);
	}
}
