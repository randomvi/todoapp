package com.todoapi.repositories;

import com.todoapi.models.Category;
import com.todoapi.models.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
    Page<Todo> findByTitleContainsAndCategoryId(String title, long id, Pageable pageable);
    Page<Todo> findByTitleContains(String title, Pageable pageable);
}
