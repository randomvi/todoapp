import { BrowserModule } from '@angular/platform-browser';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoMainComponent } from './components/todo-main/todo-main.component';
import { TodoListComponent } from './components/todo-main/todo-list/todo-list.component';
import { TodoDetailComponent } from './components/todo-main/todo-detail/todo-detail.component';
import { TodoService } from './services/todo.service';
import { CategoryService } from './services/category.service';
import { CategoryMainComponent } from './components/category-main/category-main.component';
import { PageNotFoundComponent } from './components/shared/page-not-found/page-not-found.component';
import { TodoCreateComponent } from './components/todo-main/todo-create/todo-create.component';
import { TodoEditComponent } from './components/todo-main/todo-edit/todo-edit.component';
import { TodoListViewComponent } from './components/todo-main/todo-list/todo-list-view/todo-list-view.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    TodoMainComponent,
    TodoListComponent,
    TodoDetailComponent,
    CategoryMainComponent,
    PageNotFoundComponent,
    TodoCreateComponent,
    TodoEditComponent,
    TodoListViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    BrowserAnimationsModule
  ],
  providers: [TodoService, CategoryService, { provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
