import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Todo } from 'src/app/models/todo';
import { Category } from 'src/app/models/category';
import { TodoService } from 'src/app/services/todo.service';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'todoapp-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.css']
})
export class TodoEditComponent implements OnInit {

  isVisible: boolean;
  isSaving: boolean;
  @Output() closeEvent = new EventEmitter();
  @Input() todoCategories: Category[];
  @Input() visible: boolean;
  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  constructor(protected todoService: TodoService, private message: NzMessageService) { }

  ngOnInit() {
  }

  handleCancel() {
    this.closeEvent.emit();
  }

  handleOk() {
    this.isSaving = true;
    this.todoService.update(this.todoService.current).subscribe((data) => {
      this.isSaving = false;
      if (data != null) {
      this.message.info('Todo Updated Successfully!');
      }
      this.closeEvent.emit();
    }, (error) => {
      this.message.error(error);
      this.isSaving = true;
    });
  }

}
