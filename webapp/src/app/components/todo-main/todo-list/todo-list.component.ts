import { Component, OnInit, Input } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { Todo } from 'src/app/models/todo';
import { CategoryService } from 'src/app/services/category.service';
import { Category } from 'src/app/models/category';
import { Router, ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'todoapp-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

 @Input() todoCategories: Category[];
 isLoading: boolean;
 categories: Category[];
 searchTerm: string;
 category: Category = new Category();
 filterBy = 1;
 page = 0;
 totalPages = 0;

 editModalVisible: boolean;
 createModalVisible: boolean;

  constructor(private router: Router, private route: ActivatedRoute,
              protected todoService: TodoService, private categoryService: CategoryService, private message: NzMessageService) { }

  ngOnInit() {
    this.loadData();
    this.categoryService.findAll()
    .subscribe((response: Category[]) => {
      this.categories = response;
    });

  }

  loadData() {
    this.isLoading = true;

    this.todoService.findAllBy(this.getCategoryId(this.category) || 0, this.searchTerm)
    .subscribe((response: any) => {
      if (response) {
          this.todoService.list = response.content;
          this.todoService.pagination = {
            page: response.number,
            pageSize: response.size,
            totalElements: response.totalElements,
            totalPages: response.totalPages
          };
      }
      this.isLoading = false;
    }, (error) => {
      this.isLoading = false;
      this.message.error(error);
    });

  }

  getCategoryId(obj) {
      if (obj == null) {
        return 0;
      }
      return obj.id;
  }

  onDeleteTodo($event: Todo) {
    this.todoService.delete($event.id).subscribe((data) => {
      if (data != null) {
        this.message.info('Todo Deleted Successfully');
      }
      this.ngOnInit();
    }, (error) => {
      this.message.error(error);
    });
  }

  onCloseCreateModal() {
    this.createModalVisible = false;
  }

  onCloseEditModal() {
    this.editModalVisible = false;
  }

  onOpenCreateModal() {
    this.createModalVisible = true;
  }

  onOpenEditModal($event) {
    this.todoService.current = $event;
    this.editModalVisible = true;
  }

  onSearch() {
    this.loadData();
  }

  onSelectTodo(selected: Todo) {
    this.todoService.current = selected;
    this.router.navigate(['/todos/details',  selected.id]);
  }

  onPageChange() {
    this.loadData();
  }

}
