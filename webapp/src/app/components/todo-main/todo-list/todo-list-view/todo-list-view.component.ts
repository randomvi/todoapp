import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'todoapp-todo-list-view',
  templateUrl: './todo-list-view.component.html',
  styleUrls: ['./todo-list-view.component.css']
})
export class TodoListViewComponent implements OnInit {

  loadingMore = false;
  @Input() data: Todo[];
  @Input() isLoading: boolean;
  @Output() selectEvent = new EventEmitter<Todo>();
  @Output() editEvent = new EventEmitter<Todo>();
  @Output() deleteEvent = new EventEmitter<Todo>();
  @Output() pageChangeEvent = new EventEmitter();

  constructor(protected todoService: TodoService) { }

  ngOnInit() {

  }

  edit(todo: Todo): void {
    this.editEvent.emit(todo);
  }

  delete(todo: Todo) {
    this.deleteEvent.emit(todo);
  }

  openTodo(todo: Todo) {
    this.selectEvent.emit(todo);
  }

  onPagination($event) {
    this.todoService.pagination.page = $event;
    this.pageChangeEvent.emit();
  }

}
