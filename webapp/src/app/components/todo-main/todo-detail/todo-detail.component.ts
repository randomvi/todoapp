import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodoService } from 'src/app/services/todo.service';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'todoapp-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {

  params: any;

  constructor(private route: ActivatedRoute, protected todoService: TodoService, private message: NzMessageService) {
    this.route.params.subscribe( params => {
      this.params = params.id;
    });
  }

  ngOnInit() {
  }

  onChange() {
    this.todoService.update(this.todoService.current).subscribe(data => {
      if (data != null) {
        this.message.info('Todo Updated Successfully!');
      }
      this.todoService.findAllBy(0, '').subscribe();
    }, (error) => {
      this.message.error(error);
    });
  }

  dateFormater(date: any) {
    return (new Date(date).toDateString() + ' ' + new Date(date).toLocaleTimeString());
  }

}
