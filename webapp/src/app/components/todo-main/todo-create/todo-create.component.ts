import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Todo } from 'src/app/models/todo';
import { Category } from 'src/app/models/category';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'todoapp-todo-create',
  templateUrl: './todo-create.component.html',
  styleUrls: ['./todo-create.component.css']
})
export class TodoCreateComponent implements OnInit {

  validateForm: FormGroup;
  isSaving: boolean;
  @Input() visible: boolean;
  @Input() todoCategories: Category[];
  @Output() closeEvent = new EventEmitter();
  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  constructor(private fb: FormBuilder, private todoService: TodoService, private message: NzMessageService) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      title: [null, [Validators.required]],
      category: [null],
      dueDate: [null, [Validators.required]],
      description: [null],
    });
  }

  handleCancel() {
    this.closeEvent.emit();
  }

  submitForm(): void {

    const todo: Todo = new Todo(...this.validateForm.value);

    this.isSaving = true;
    this.todoService.create(todo).subscribe((data) => {

      this.isSaving = false;
      if (data != null) {
      this.todoService.list = [data, ...this.todoService.list];
      this.message.info('Todo Created Successfully');
      this.resetForm();
      }
      this.closeEvent.emit();
    }, (error) => {
      this.message.error(error);
      this.isSaving = false;
    });

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

  }

  resetForm() {
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsPristine();
      this.validateForm.controls[key].updateValueAndValidity();
    }
  }

}
