import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { NzInputDirective, NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'todoapp-category-main',
  templateUrl: './category-main.component.html',
  styleUrls: ['./category-main.component.css']
})
export class CategoryMainComponent implements OnInit {

  categoryName: string;
  categories: Category[] = [];
  i = 0;
  editId: string | null;
  @ViewChild(NzInputDirective, { read: ElementRef }) inputElement: ElementRef;

  constructor(private categoryService: CategoryService, private message: NzMessageService) { }

  ngOnInit() {

    this.categoryService.findAll()
    .subscribe((response: Category[]) => {
      this.categories = response;
    });

  }

  @HostListener('window:click', ['$event'])
  handleClick(e: MouseEvent): void {
    if (this.editId && this.inputElement && this.inputElement.nativeElement !== e.target) {
      this.editId = null;
    }
  }

  startEdit(id: string, event: MouseEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.editId = id;
  }

  onBlur(data: Category) {
    this.categoryService.update(data).subscribe((data) => {
      if (data != null) {
        this.message.info('Category Updated Successfully');
      }
    });
  }

  addRow(): void {
  this.categoryService.create(new Category({ name: 'Enter Category Name Here', id: 0 }))
  .subscribe(category => {
    this.ngOnInit();
  });
  }

  deleteCategory(id) {
    this.categoryService.delete(id).subscribe(() => {
      this.message.info('Category Deleted Successfully');
      this.ngOnInit();
    });
  }

}
