import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoMainComponent } from './components/todo-main/todo-main.component';
import { TodoDetailComponent } from './components/todo-main/todo-detail/todo-detail.component';
import { CategoryMainComponent } from './components/category-main/category-main.component';
import { PageNotFoundComponent } from './components/shared/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'todos', component: TodoMainComponent,
    children: [
      {
        path: 'details/:id',
        component: TodoDetailComponent
      }
    ]
  },
  { path: 'categories', component: CategoryMainComponent, },
  { path: '', redirectTo: 'todos', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
