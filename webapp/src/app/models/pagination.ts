export class Pagination {

  page: number;
  totalElements: number;
  pageSize: number;
  totalPages: number;

  constructor(values: Object = {}) {
    Object.assign(this, values);
    this.page = 0;
    this.totalElements = 0;
    this.pageSize = 5;
    this.totalPages = 0;
  }

}
