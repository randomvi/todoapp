import { Todo } from './todo';

describe('Todo', () => {
  it('should create an instance', () => {
    expect(new Todo()).toBeTruthy();
  });

  it('should accept values in the constructor', () => {

    const todo = new Todo({
      title: 'write code',
      description: 'write that cool todo application',
      isComplete: true
    });

    expect(todo.title).toEqual('hello');
    expect(todo.description).toEqual('write that cool todo application');
    expect(todo.isComplete).toEqual(true);
  });
});
