import { Category } from './category';

export class Todo {

  id: number;
  title: string;
  description: string;
  isComplete: boolean;
  dueDate: Date;
  category: Category;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
