import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Category } from '../models/category';
import { NzMessageService } from 'ng-zorro-antd';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  public resourceUrl = 'http://localhost:8080/api/';

  constructor(private http: HttpClient) {}

  findAll() {
    return this.http.get(this.resourceUrl + 'categories').pipe(catchError(this.errorHandler));
  }

  create(category: Category): Observable<Category> {
    return this.http.post<Category>(this.resourceUrl + 'categories', category, httpOptions)
    .pipe(catchError(this.errorHandler));
  }

  update(category: Category): Observable<Category> {
    return this.http.put<Category>(this.resourceUrl + 'categories/' + category.id, category, httpOptions)
    .pipe(catchError(this.errorHandler));
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}categories/${id}`, httpOptions)
    .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Server Error');
  }

}
