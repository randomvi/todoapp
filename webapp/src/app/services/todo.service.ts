import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Todo } from '../models/todo';
import { Pagination } from '../models/pagination';
import { NzMessageService } from 'ng-zorro-antd';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  current: Todo = new Todo();
  list: Todo[] = [];

  pagination: Pagination = new Pagination();

  public resourceUrl = 'http://localhost:8080/api/';

  constructor(private http: HttpClient) {}

  findAll() {
    return this.http.get(this.resourceUrl + 'todos').pipe(catchError(this.errorHandler));
  }

  findAllBy(category = 0, title = '') {
    return this.http.get(this.resourceUrl + 'todos/paged?page=' + this.pagination.page + '&title=' + title + '&category=' + category)
    .pipe(catchError(this.errorHandler));
  }

  create(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(this.resourceUrl + 'todos', todo, httpOptions).pipe(catchError(this.errorHandler));
  }

  update(todo: Todo): Observable<Todo> {
    return this.http.put<Todo>(this.resourceUrl + 'todos/' + todo.id, todo, httpOptions).pipe(catchError(this.errorHandler));
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}todos/${id}`, httpOptions).pipe(catchError(this.errorHandler));
  }

  find(id: number): Observable<Todo> {
    return this.http.get<Todo>(this.resourceUrl + 'todos/' + id).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Server Error');
   }

}
